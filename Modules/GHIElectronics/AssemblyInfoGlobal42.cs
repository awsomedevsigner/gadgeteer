﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyVersion("4.2.109.0")]
[assembly: AssemblyFileVersion("4.2.109.0")]
[assembly: AssemblyInformationalVersion("4.2.109.0")]

[assembly: AssemblyCompany("GHI Electronics")]
[assembly: AssemblyCopyright("Copyright © GHI Electronics 2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCulture("")]